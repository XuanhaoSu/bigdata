# Big Data in Finance 

## Part II: CRSP and Compustat

### Author: Lira Mota 

### Version: 0.1

This repository contains class material for Part II of Big Data in Finance (Spring 2019).

# Topics
1. Compustat
2. CRSP
3. Factor Investing